package connect;

public interface ServerApplication {
    
    /**
     * 
     * @return The current humidity
     */
    int getHumidity();
    
    /**
     * 
     * @return The current pump state (STOP, MIN, MED, MAX)
     */
 
    
    String getPumpState();
    
    /**
     * Sets the humidity.
     * 
     * @param humidity
     *          The current humidity
     */
    void setHumidity(int humidity);
    
    /**
     * Sets the pump state.
     * 
     * @param pump
     *          The enum pump state
     */
    void setPumpState(PumpState pump);
   
    /**
     * Initialize the pump state.
     * 
     * @param Humidity
     *          The current humidity level
     * @param pump
     *          The current pump state
     */
    void init(int Humidity);
    
    /**
     * Updates the pump status based on the current humidity level.
     * 
     * @param humidity
     *          The current humidity
     */
    void update(int humidity);
}
