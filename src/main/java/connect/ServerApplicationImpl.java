package connect;

public class ServerApplicationImpl implements ServerApplication {
    
    private int humidity;
    private PumpState pumpState;
    private static final int DELTA = 5;

    @Override
    public int getHumidity() {
        return this.humidity;
    }

    @Override
    public String getPumpState() {
        switch (pumpState) {
        case STOP:
            return "STOP";
        case MIN:
            return "MIN";
        case MED:
            return "MED";
        case MAX:
            return "MAX";
        default:
            return "ERROR";
        }
    }
    
    @Override
    public void setHumidity(int humidity) {
        this.humidity = humidity;      
    }

    @Override
    public void setPumpState(PumpState pump) {
        this.pumpState = pump;      
    }

    @Override
    public void init(int humidity) {
        setHumidity(humidity);
        updateStatus();
    }

    @Override
    public void update(int humidity) {
        setHumidity(humidity);
        switch (pumpState) {
        case STOP:
            if (humidity <= 30)
                updateStatus();
            break;
        case MIN:
            if (humidity > 30+DELTA || humidity <= 20-DELTA)
                updateStatus();
            break;
        case MED:
            if (humidity > 20+DELTA || humidity <= 10-DELTA)
                updateStatus();
            break;
        case MAX:
            if (humidity > 10+DELTA)
                updateStatus();
            break;
        }
    }
    
    private void updateStatus() {
        if (humidity > 30) {
            this.pumpState = PumpState.STOP;
        } else if (humidity <= 30 && humidity > 20) {
            this.pumpState = PumpState.MIN;
        } else if (humidity <= 20 && humidity > 10) {
            this.pumpState = PumpState.MED;
        } else if (humidity <= 10) {
            this.pumpState = PumpState.MAX;
        }
    }




}
