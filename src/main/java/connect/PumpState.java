package connect;

public enum PumpState {
        STOP,
        MIN,
        MED,
        MAX
}