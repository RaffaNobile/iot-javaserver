package connect;


import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SocketConnectionImpl implements SocketConnection {

	Socket socket;
	ServerSocket serverSocket;

	@Override
	public void closeSocket() throws IOException {
		socket.close();

	}

	 @SuppressWarnings("unused")
	public void startServer(int port) {
	        final ExecutorService clientProcessingPool = Executors.newFixedThreadPool(10);

	        Runnable serverTask = new Runnable() {
	            @Override
	            public void run() {
	        		try {

	        			int port = 6868;

	        			@SuppressWarnings("resource")
						ServerSocket serverSocket = new ServerSocket(port);
	        			System.out.println("Server Started and listening to the port 6868");

	        			// Server is running always. This is done using this while(true) loop
	        			while (true) {
	        				//
	        				socket = serverSocket.accept();
	        				OutputStream os = socket.getOutputStream();
	        				OutputStreamWriter osw = new OutputStreamWriter(os);
	        				BufferedWriter bw = new BufferedWriter(osw);				
	        				String number = "PORCODIO-NEGRO-MADONNA"; // data from Arduino
	        				String sendMessage = number + "\n";
	        				bw.write(sendMessage);
	        				bw.flush();
	        				System.out.println("Message sent to the client : " + sendMessage);
	        				//

	        			}
	        		} catch (Exception e) {
	        			e.printStackTrace();
	        		} finally {
	        			try {
	        				socket.close();
	        			} catch (Exception e) {
	        			}
	        		}
	            }
	        };
	        Thread serverThread = new Thread(serverTask);
	        serverThread.start();

	    }

	public String sendMsgSocket(String msg) throws IOException {
		return msg;
		
	}

	@Override
	public Socket returnSocket() {
		return this.socket;
		
	}
}
