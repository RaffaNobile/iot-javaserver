package connect;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;

public class HTTPConnect extends AbstractVerticle {
	private static int portNo = 6868;
	static ArduinoConnect ac = new ArduinoConnectImpl();
	static SocketConnection socketC = new SocketConnectionImpl();
	Vertx vertx = Vertx.vertx();


	public static void main(String[] args) throws Exception {
		//ac.connect();
		socketC.startServer(portNo);
		Runner.runExample(HTTPConnect.class);
	}

	public void start() throws Exception{

		System.out.println("IN ASCOLTO");
		/* HTTP HANDLE VERTX */
		ServerApplication getFromESP = new ServerApplicationImpl(); /* Server Function to communicate with ARDUINO */
		getFromESP.init(getFromESP.getHumidity()); /* Init variables */

		/* Start Server */
		HttpServer server = vertx.createHttpServer();

		/* Handle Request */

		server.requestHandler(request -> {
			request.params()
					.forEach(p -> getFromESP.setHumidity(Integer.valueOf(p.getValue()))); /* Request Humidity value */
			getFromESP.update(getFromESP.getHumidity());
			HttpServerResponse response = request.response();
			ac.getChannel().sendMsg(getFromESP.getPumpState());
			response.putHeader("content-type", "text/html");
			response.setStatusCode(200);
			response.end("CIAO" + getFromESP.getPumpState());
			
		}).listen(8080);
	}
}