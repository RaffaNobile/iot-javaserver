package connect;

import serial.comm.SerialCommChannel;

public interface ArduinoConnect {
	
	public void connect() throws Exception;
	/* Connect arduino */
	
	public SerialCommChannel getChannel();

}
