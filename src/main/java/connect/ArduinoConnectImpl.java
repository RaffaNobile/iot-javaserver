package connect;

import serial.comm.SerialCommChannel;

public class ArduinoConnectImpl implements ArduinoConnect
{
	SerialCommChannel channel;

	@Override
	public void connect() throws Exception {
		/* ARDUINO SERIAL */
		channel = new SerialCommChannel("COM4", 9600);
		System.out.println("Waiting Arduino for rebooting...");
		Thread.sleep(4000);
		System.out.println("Ready.");
	}

	@Override
	public SerialCommChannel getChannel() {
		return channel;
	}

	
}
