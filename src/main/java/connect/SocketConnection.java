package connect;

import java.io.IOException;
import java.net.Socket;

public interface SocketConnection {

	
	public void closeSocket() throws IOException;
	/*close socket connection*/
	
	public void startServer(int port);
	
	public String sendMsgSocket(String msg) throws IOException;
	
	public Socket returnSocket();
}
